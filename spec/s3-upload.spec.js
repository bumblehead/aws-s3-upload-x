// Filename: s3-copy.spec.js  
// Timestamp: 2019.07.10-15:44:55 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

const upload = require('../src/s3-upload');

describe('s3-upload', () => {
  it('should be a function', () => {
    expect(typeof upload).toBe('function');
  });
});

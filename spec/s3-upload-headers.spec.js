// Filename: s3-upload-headers.spec.js
// Timestamp: 2019.07.11-13:43:20 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

const uploadHeaders = require('../src/s3-upload-headers');

describe('uploadHeaders.codePointSort', () => {
  it('should return sorted headers', () => {
    expect(uploadHeaders.codePointSort([
      'name-c:value',
      'name-a:value',
      'host:localhost'
    ]).join()).toBe('host:localhost,name-a:value,name-c:value');
  });
});

describe('uploadHeaders.getCanonicalHeaders', () => {
  it('should return canonical headers', () => {
    expect(uploadHeaders.getCanonicalHeaders({
      host : 'host',
      amzdatets : 'amzdatets'
    })).toBe('host:host\nx-amz-date:amzdatets\n');
  });

  it('should return canonical headers, with content_type', () => {
    expect(uploadHeaders.getCanonicalHeaders({
      host : 'host',
      amzdatets : 'amzdatets',
      content_type : 'type'
    })).toBe('content-type:type\nhost:host\nx-amz-date:amzdatets\n');
  });

  it('should return canonical headers, with content_md5', () => {
    expect(uploadHeaders.getCanonicalHeaders({
      host : 'host',
      amzdatets : 'amzdatets',
      payloadmd5 : 'md5'
    })).toBe('content-md5:md5\nhost:host\nx-amz-date:amzdatets\n');
  });

  it('should return canonical headers, with x-amz-content-sha256', () => {
    expect(uploadHeaders.getCanonicalHeaders({
      host : 'host',
      amzdatets : 'amzdatets',
      payloadsha : 'sha'
    })).toBe('host:host\nx-amz-content-sha256:sha\nx-amz-date:amzdatets\n');
  });
});

describe('uploadHeaders.getSignedHeaders', () => {
  it('should return signed headers', () => {
    expect(uploadHeaders.getSignedHeaders({
      host : 'host',
      amzdatets : 'amzdatets'
    })).toBe('host;x-amz-date');
  });

  it('should return signed headers, with content_type', () => {
    expect(uploadHeaders.getSignedHeaders({
      host : 'host',
      amzdatets : 'amzdatets',
      content_type : 'type'
    })).toBe('content-type;host;x-amz-date');
  });

  it('should return signed headers, with content_md5', () => {
    expect(uploadHeaders.getSignedHeaders({
      host : 'host',
      amzdatets : 'amzdatets',
      payloadmd5 : 'md5'
    })).toBe('content-md5;host;x-amz-date');
  });

  it('should return signed headers, with x-amz-content-sha256', () => {
    expect(uploadHeaders.getSignedHeaders({
      host : 'host',
      amzdatets : 'amzdatets',
      payloadsha : 'sha'
    })).toBe('host;x-amz-content-sha256;x-amz-date');      
  });
});

describe('uploadHeaders.getCredentialScope', () => {
  it('should return credential scope', () => {
    expect(uploadHeaders.getCredentialScope({
      datetimets : 'datetimets',
      service : 'service',
      region : 'region'
    })).toBe('datetimets/region/service/aws4_request');
  });
});

describe('uploadHeaders.getAuthHeaders', () => {
  it('should return auth headers', () => {
    expect(uploadHeaders.getAuthHeaders({
      algorithm : 'SHA256',
      access_key : 'key',
      signature : 'sig',
      host : 'host',
      amzdatets : 'amzdatets',
      datetimets : 'datetimets',
      service : 'service',
      region : 'region'
      // eslint-disable-next-line max-len
    })).toBe('SHA256 Credential=key/datetimets/region/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=sig');
  });
});

describe('uploadHeaders.getStringToSign', () => {
  it('should return string to sign', () => {
    expect(uploadHeaders.getStringToSign({
      algorithm : 'SHA256',
      method : 'POST',
      canonical_uri : 'canonical_uri',
      querystring : '?query=string',
      payloadsha : '',
      access_key : 'key',
      signature : 'sig',
      host : 'host',
      amzdatets : 'amzdatets',
      datetimets : 'datetimets',
      service : 'service',
      region : 'region'
    })).toBe([
      'SHA256',
      'amzdatets',
      'datetimets/region/service/aws4_request',
      'b069c4ea38025f0bb252e843cdf640bdaca6a8035cbdc15fcf0428f91ee4c310'
    ].join('\n'));
  });
});

describe('uploadHeaders.getCommonHeaders', () => {
  it('should return string to sign', () => {
    expect(JSON.stringify(uploadHeaders.getCommonHeaders({
      algorithm : 'SHA256',
      method : 'POST',
      canonical_uri : 'canonical_uri',
      querystring : '?query=string',
      payloadsha : '',
      access_key : 'key',
      signature : 'sig',
      host : 'host',
      amzdatets : 'amzdatets',
      datetimets : 'datetimets',
      service : 'service',
      region : 'region'
    }), null, '  ')).toBe(JSON.stringify({
      // eslint-disable-next-line max-len
      authorization : 'SHA256 Credential=key/datetimets/region/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=sig',
      'x-amz-date' : 'amzdatets'
    }, null, '  '));
  });

  it('should return string to sign, with payloadsha', () => {
    expect(JSON.stringify(uploadHeaders.getCommonHeaders({
      algorithm : 'SHA256',
      method : 'POST',
      canonical_uri : 'canonical_uri',
      querystring : '?query=string',
      payloadsha : 'payloadsha',
      access_key : 'key',
      signature : 'sig',
      host : 'host',
      amzdatets : 'amzdatets',
      datetimets : 'datetimets',
      service : 'service',
      region : 'region'
    }), null, '  ')).toBe(JSON.stringify({
      // eslint-disable-next-line max-len
      authorization : 'SHA256 Credential=key/datetimets/region/service/aws4_request, SignedHeaders=host;x-amz-content-sha256;x-amz-date, Signature=sig',
      'x-amz-date' : 'amzdatets',
      'x-amz-content-sha256' : 'payloadsha'
    }, null, '  '));
  });

  it('should return string to sign, with payloadmd5', () => {
    expect(JSON.stringify(uploadHeaders.getCommonHeaders({
      algorithm : 'SHA256',
      method : 'POST',
      canonical_uri : 'canonical_uri',
      querystring : '?query=string',
      payloadsha : '',
      payloadmd5 : 'md5',
      access_key : 'key',
      signature : 'sig',
      host : 'host',
      amzdatets : 'amzdatets',
      datetimets : 'datetimets',
      service : 'service',
      region : 'region'
    }), null, '  ')).toBe(JSON.stringify({
      // eslint-disable-next-line max-len
      authorization : 'SHA256 Credential=key/datetimets/region/service/aws4_request, SignedHeaders=content-md5;host;x-amz-date, Signature=sig',
      'x-amz-date' : 'amzdatets',
      'Content-MD5' : 'md5'
    }, null, '  '));
  });
});

// Filename: signer.spec.js  
// Timestamp: 2019.07.15-12:28:54 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>  

const signer = require('../test-service/signer');

describe('signer.createSigningKey', () => {
  it('should return the correct key', () => {
    // eslint-disable-next-line max-len
    // https://docs.aws.amazon.com/general/latest/gr/signature-v4-examples.html#signature-v4-examples-javascript
    expect(signer.createSigningKey({
      key : 'wJalrXUtnFEMI/K7MDENG+bPxRfiCYEXAMPLEKEY',
      date : '20120215',
      regionName : 'us-east-1',
      serviceName : 'iam'
    }).toString()).toBe(
      'f4780e2d9f65fa895f9c67b32ce1baf0b0d8a43505a000a1a9e090d414db404d');
  });

  // eslint-disable-next-line max-len
  // https://docs.aws.amazon.com/general/latest/gr/sigv4-calculate-signature.html
  it('should return the correct key', () => {
    expect(signer.createSigningKey({
      key : 'wJalrXUtnFEMI/K7MDENG+bPxRfiCYEXAMPLEKEY',
      date : '20150830',
      regionName : 'us-east-1',
      serviceName : 'iam'
    }).toString()).toBe(
      'c4afb1cc5771d871763a393e44b703571b55cc28424d1a5e86da6ed3c154a4b9');
  });
});

describe('signer', () => {
  it('should return the correct signature', () => {
    expect(signer([
      'AWS4-HMAC-SHA256',
      '20150830T123600Z',
      '20150830/us-east-1/iam/aws4_request',
      'f536975d06c0309214f805bb90ccff089219ecd68b2577efef23edd43b7e1a59'
    ].join('\n'), {
      key : 'wJalrXUtnFEMI/K7MDENG+bPxRfiCYEXAMPLEKEY',
      date : '20150830',
      regionName : 'us-east-1',
      serviceName : 'iam'
    })).toBe(
      '5d672d79c15b13162d9279b0855cfba6789a8edb4c82c400e06b5924a6f2b5d7');
  });

  it('should return the correct signature, when date instance is given', () => {
    expect(signer([
      'AWS4-HMAC-SHA256',
      '20150830T123600Z',
      '20150830/us-east-1/iam/aws4_request',
      'f536975d06c0309214f805bb90ccff089219ecd68b2577efef23edd43b7e1a59'
    ].join('\n'), {
      key : 'wJalrXUtnFEMI/K7MDENG+bPxRfiCYEXAMPLEKEY',
      date : new Date(1440959427000), // 2015.8.30
      regionName : 'us-east-1',
      serviceName : 'iam'
    })).toBe(
      '5d672d79c15b13162d9279b0855cfba6789a8edb4c82c400e06b5924a6f2b5d7');
  });
});

// Filename: s3-upload.js
// Timestamp: 2019.07.11-12:58:25 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

const signingBlock = require('./s3-upload-signing-block');
const uploadReq = require('./s3-upload-req');

const upload = (opts, fn) => {
  signingBlock.create({
    file : opts.file,
    upload_path : `https://${opts.Bucket}.s3-accelerate.amazonaws.com/file.mp4`,
    canonical_uri : `${opts.Bucket}.s3-accelerate.amazonaws.com`,
    host : `${opts.Bucket}.s3-accelerate.amazonaws.com`,
    access_key : opts.AccessKey,
    region : opts.Region
  }, (err, block) => {
    uploadReq(opts, block, (err, res) => {
      console.log({
        err, res, block, file : opts.file, fn
      });
    });
  });
};

const uploadPromise = opts => new Promise((resolve, reject) => {
  upload(opts, (err, res) => err
    ? reject(err)
    : resolve(res));
});

module.exports = uploadPromise;

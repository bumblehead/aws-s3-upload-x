// Filename: s3-upload-req.js
// Timestamp: 2019.07.11-13:33:27 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

const signingBlock = require('./s3-upload-signing-block');
const headers = require('./s3-upload-headers');

const req = (opts, block, fn) => {
  opts.sign(headers.getStringToSign(block), signature => {
    block = signingBlock.setSignature(block, signature);

    console.log({
      block,
      url : signingBlock.getEndpointUri(block),
      method : block.method,
      headers : headers.getCommonHeaders(block)
    });
    fetch(signingBlock.getEndpointUri(block), {
      mode : 'cors',
      method : block.method,
      headers : headers.getCommonHeaders(block),
      body : block.payload
    }).then(res => {
      res.text().then(text => {
        console.log({ res, text, fn });
      });
    });
  });
};

const upload = (opts, block, fn) => {
  block = signingBlock.setStateSinglePart(block);

  req(opts, block, (err, res) => {
    console.log('req result', { err, res, fn });
  });
};

module.exports = upload;

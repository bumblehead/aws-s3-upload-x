// Filename: s3-upload-headers.js
// Timestamp: 2019.07.11-13:42:05 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

const sha256 = require('js-sha256');

// Sort the parameter names by character code point in ascending order.
// For example, a parameter name that begins with the uppercase letter F
// precedes a parameter name that begins with a lowercase letter b.
const codePointSort = list => list.sort();

// eslint-disable-next-line max-len
// https://docs.aws.amazon.com/general/latest/gr/sigv4-create-canonical-request.html
//
// ex, "content-type:$content_type
//      host:$host
//      x-amz-date:$amz_date
//      x-amz-target:$amz_target
//      "
//
const getCanonicalHeaders = block => {
  const headers = [];

  headers.push(`host:${block.host}`);
  headers.push(`x-amz-date:${block.amzdatets}`);

  if (block.content_type)
    headers.push(`content-type:${block.content_type}`);

  if (block.payloadmd5)
    headers.push(`content-md5:${block.payloadmd5}`);

  if (block.payloadsha &&
      block.payloadsha !== 'UNSIGNED-PAYLOAD')
    headers.push(`x-amz-content-sha256:${block.payloadsha}`);

  return `${codePointSort(headers).join('\n')}\n`;
};

// ex, "content-type;host;x-amz-date;x-amz-target"
//     "content-md5;content-type;host;x-amz-date"
//     "content-type;host;x-amz-content-sha256;x-amz-date"
//
const getSignedHeaders = block => {
  let headers = [];

  headers.push('host');
  headers.push('x-amz-date');

  if (block.content_type)
    headers.push('content-type');

  if (block.payloadmd5)
    headers.push('content-md5');

  if (block.payloadsha &&
      block.payloadsha !== 'UNSIGNED-PAYLOAD')
    headers.push('x-amz-content-sha256');

  return codePointSort(headers).join(';');
};

const getCredentialScope = block =>
  ':0/:1/:2/:3'
    .replace(/:0/, block.datetimets)
    .replace(/:1/, block.region)
    .replace(/:2/, block.service)
    .replace(/:3/, 'aws4_request');

// ex, "SHA256 Credential=:key/:scope, SignedHeaders=:headers, Signature=:sig"
//
const getAuthHeaders = block =>
  ':0 Credential=:1/:2, SignedHeaders=:3, Signature=:4'
    .replace(/:0/, block.algorithm)
    .replace(/:1/, block.access_key)
    .replace(/:2/, getCredentialScope(block))
    .replace(/:3/, getSignedHeaders(block))
    .replace(/:4/, block.signature);


const getStringToSign = block => {
  const { amzdatets, algorithm, method } = block;

  // ************* TASK 1: CREATE A CANONICAL REQUEST *************
  // eslint-disable-next-line max-len
  // http://docs.aws.amazon.com/general/latest/gr/sigv4-create-canonical-request.html
  //
  // Step 1 is to define the verb (GET, POST, etc.)--already done.
  //
  // Step 2: Create canonical URI--the part of the URI from domain to query
  //
  // string (use '/' if no path)
  const canonical_uri = `/${block.canonical_uri.replace(/^\//, '')}`;

  // Step 3: Create the canonical query string. In this example (a GET request),
  // request parameters are in the query string. Query string values must
  // be URL-encoded (space=%20). The parameters must be sorted by name.
  // For this example, the query string is pre-formatted in the
  // request_parameters variable.
  const canonical_querystring = block.querystring;

  // Step 4: Create the canonical headers and signed headers. Header names
  // must be trimmed and lowercase, and sorted in code point order from
  // low to high. Note that there is a trailing \n.
  const canonical_headers = getCanonicalHeaders(block);

  // Step 5: Create the list of signed headers. This lists the headers
  // in the canonical_headers list, delimited with ";" and in alpha order.
  // Note: The request can include any headers; canonical_headers and
  // signed_headers lists those that you want to be included in the
  // hash of the request. "Host" and "x-amz-date" are always required.
  const signed_headers = getSignedHeaders(block);

  // Step 6: Create payload hash (hash of the request body content). For GET
  // requests, the payload is an empty string ("").
  const payload_hash = block.payloadsha;

  // Step 7: Combine elements to create canonical request
  const canonical_request = [
    method,
    canonical_uri,
    canonical_querystring,
    canonical_headers,
    signed_headers,
    payload_hash
  ].join('\n');

  // ************* TASK 2: CREATE THE STRING TO SIGN*************
  // Match the algorithm to the hashing algorithm you use, either SHA-1 or
  // SHA-256 (recommended)
  const credential_scope = getCredentialScope(block);

  return ':0\n:1\n:2\n:3'
    .replace(/:0/, algorithm)
    .replace(/:1/, amzdatets)
    .replace(/:2/, credential_scope)
    .replace(/:3/, sha256(canonical_request));
};

//
// https://docs.aws.amazon.com/AmazonS3/latest/API/RESTCommonRequestHeaders.html
//
const getCommonHeaders = block => {
  const headers = {};

  // ************* TASK 4: ADD SIGNING INFORMATION TO THE REQUEST *************
  // The signing information can be either in a query string value or in
  // a header named Authorization. This code shows how to use a header.
  // Create authorization header and add to request headers
  headers.authorization = getAuthHeaders(block);
  headers['x-amz-date'] = block.amzdatets;

  // eslint-disable-next-line max-len
  // https://docs.aws.amazon.com/AmazonS3/latest/API/sig-v4-header-based-auth.html
  //
  // When you use Signature Version 4, for requests that use the Authorization
  // header, you add the x-amz-content-sha256 header in the signature
  // calculation and then set its value to the hash payload.
  //
  // this error returned if x-amz-content-sha256 not included
  //
  // <Error>
  //   <Code>InvalidRequest</Code>
  //   <Message>
  //     Missing required header for this request: x-amz-content-sha256
  //   </Message>
  //   <RequestId>9BC56BC7422F4F7D</RequestId>
  //   <HostId>
  //     fGl67QmgXpFaRR4jZqCTr3+dhUTvY/ieTUnHoXo15e8rpYKwl60rgeQsC/8YHiqBeWkutU=
  //   </HostId>
  // </Error>
  //
  if (block.payloadsha)
    headers['x-amz-content-sha256'] = block.payloadsha;

  if (block.payloadmd5)
    headers['Content-MD5'] = block.payloadmd5;

  headers['Content-Type'] = block.content_type;

  return headers;
};

module.exports = {
  codePointSort,
  getCanonicalHeaders,
  getSignedHeaders,
  getCredentialScope,
  getAuthHeaders,
  getStringToSign,
  getCommonHeaders
};

// Filename: s3-upload-signing-block.js
// Timestamp: 2019.07.11-13:35:59 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

const sha256 = require('js-sha256');

// const STATEABORT = 'ABORT';
const STATEINITIATE = 'INITIATE';
// const STATEPUTPARTS = 'PUTPARTS';
const STATEPUTSINGLE = 'PUTSINGLE';

// return QueryParamUtil.Add(signingBlock.endpoint, qs);
// should add query param qs
const getEndpointUri = block =>
  block.endpoint;

const setSignature = (block, signature) => {
  block.signature = signature;

  return block;
};

// https://docs.aws.amazon.com/general/latest/gr/sigv4-date-handling.html
//
// ex, 20150830T123600Z
const getDateTimestamp = (date = new Date()) => date
  .toISOString().replace(/[:-]|\.\d{3}/g, '').substr(0, 17);

// https://docs.aws.amazon.com/general/latest/gr/sigv4_changes.html
//
// ex, 20150830
const getCredentialScopeDateTimestamp = (date = new Date()) => date
  .toISOString().replace(/[:-]|\.\d{3}/g, '').substr(0, 8);

const create = (opts, fn) => {
  const signingBlock = {};
  const datetime = new Date();

  // use POST for straight upload
  signingBlock.uploadId = '';
  signingBlock.service = 's3';

  signingBlock.access_key = opts.access_key;
  signingBlock.endpoint = opts.upload_path; // from service...

  // canonical URI excludes the host and the query parameters.
  signingBlock.canonical_uri = 'file.mp4';

  // region ex, "us-east-1"
  signingBlock.region = opts.region;
  signingBlock.amzdatets = getDateTimestamp(datetime);
  signingBlock.datetimets = getCredentialScopeDateTimestamp(datetime);
  signingBlock.content_type = opts.file.type || 'application/octet-stream';

  // https://docs.aws.amazon.com/AmazonS3/latest/dev/transfer-acceleration.html
  //
  // host:
  //   mybucket.s3-accelerate.amazonaws.com
  //
  signingBlock.host = opts.host;
  signingBlock.algorithm = 'AWS4-HMAC-SHA256';
  signingBlock.file = opts.file;
  signingBlock.payloadmd5 = '';

  // experimental
  const reader = new FileReader();
  reader.onloadend = ({ target }) => {
    signingBlock.payload = new Uint8Array(target.result);

    // signingBlock.payload = target.result;
    fn(null, signingBlock);
  };
  reader.readAsArrayBuffer(opts.file);

  return signingBlock;
};

// https://docs.aws.amazon.com/AmazonS3/latest/API/mpUploadInitiate.html
//
// POST /ObjectName?uploads HTTP/1.1
// Host: BucketName.s3.amazonaws.com
// Date: date
// Authorization: authorization string
//  (see Authenticating Requests (AWS Signature Version 4))
const setStateMultiInitiate = signingBlock => {
  signingBlock.method = 'POST';
  signingBlock.payloadbytes = null;
  signingBlock.querystring = 'uploads=';
  // signingBlock.payloadsha = SHA256Util.GetSHA256HexStr("");
  signingBlock.payloadsha = '';
  signingBlock.state = STATEINITIATE;

  return signingBlock;
};

// PUT /example-object
// Host: example-bucket.s3.amazonaws.com
// Date:  Mon, 1 Nov 2010 20:34:56 GMT
// Authorization: authorization string
const setStateSinglePart = signingBlock => {
  signingBlock.state = STATEPUTSINGLE;

  signingBlock.method = 'PUT';
  signingBlock.querystring = '';
  signingBlock.payloadbytes = signingBlock.payload;
  signingBlock.payloadsha = sha256(signingBlock.payloadbytes);

  return signingBlock;
};

module.exports = {
  getCredentialScopeDateTimestamp,
  getDateTimestamp,
  getEndpointUri,
  setStateMultiInitiate,
  setStateSinglePart,
  setSignature,
  create
};

// Filename: build.js  
// Timestamp: 2019.07.08-12:11:02 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

const scroungejs = require('scroungejs');
const package = require('./../package.json');

scroungejs.build({
  version : package.version,
  inputpath : './test-service',

  outputpath : './test-service/www/',
  publicpath : '/www',
  iscompress : false,
  isconcat : false,
  basepagein : './test-service/index.tpl.html',
  basepage : './test-service/index.html',
  iswatch : false,
  treearr : [
    'test-service.js'
  ]
}, () => {
  console.log(':)');
});

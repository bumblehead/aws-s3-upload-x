// Filename: test-service.js
// Timestamp: 2019.07.10-12:42:42 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

const s3Upload = require('../src/s3-upload');

const setStorageInputValues = inputValues =>
  localStorage.setItem('inputValues', JSON.stringify(inputValues));

const getStorageInputValues = () =>
  JSON.parse(localStorage.getItem('inputValues') || null);

const setInputValues = inputValues => {
  if (inputValues) {
    document.forms[0].AWS_ACCESS_KEY.value = inputValues.AWS_ACCESS_KEY;
    document.forms[0].AWS_BUCKET.value = inputValues.AWS_BUCKET;
    document.forms[0].AWS_REGION.value = inputValues.AWS_REGION;
  }
};

const getInputValues = () => ({
  files : [].slice.apply(document.forms[0].file.files, [ 0, 1 ]),
  AWS_ACCESS_KEY : document.forms[0].AWS_ACCESS_KEY.value,
  AWS_BUCKET : document.forms[0].AWS_BUCKET.value,
  AWS_REGION : document.forms[0].AWS_REGION.value
});

setInputValues(getStorageInputValues());

document.forms[0].addEventListener('submit', e => {
  e.preventDefault();

  const inputValues = getInputValues();

  setStorageInputValues(inputValues);

  if (inputValues.files.length) {
    s3Upload({
      Key : `${Date.now()}-${inputValues.files[0].name}`,
      Bucket : inputValues.AWS_BUCKET,
      Region : inputValues.AWS_REGION,
      AccessKey : inputValues.AWS_ACCESS_KEY,

      file : inputValues.files[0],
      info : msg => console.info(msg),
      error : msg => console.error(msg),
      partsize : 50000000, // default is 50000000 bytes (50MB)
      maxparts : 5, // default max number concurrent requests is 5
      sign : (string, fn) => fetch(`/sign?stringtosign=${string}`)
        .then(res => res.json())
        .then(res => fn(res.signedstring))
    }).then(res => {
      console.log(res);
    }).catch(err => {
      console.error(err);
    });
  }
});

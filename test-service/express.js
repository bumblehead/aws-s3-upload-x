// Filename: express.js
// Timestamp: 2019.07.09-17:08:43 (last modified)

const express = require('express'),
      http = require('http'),
      path = require('path'),
      fs = require('fs'),
      sign = require('./signer'),
      port = 7474,
      app = express();

const readAwsFile = (fn, filepath = path.join(__dirname, '.env')) => {
  fs.readFile(filepath, 'utf-8', (err, data) => {
    if (err) return fn(err);

    fn(null, data.match(/\w*=[\S]*/gmi)
      .reduce((obj, match) => Object.assign(obj, {
        [match.split('=')[0]] : match.split('=')[1]
      }), {}));
  });
};

app.use('/', express.static(path.join(__dirname, '/')));
app.use('/www', express.static(path.join(__dirname, '/www')));

app.get('/sign', (req, res) => {
  const { stringtosign } = req.query;

  readAwsFile((err, aws) => {
    if (err) throw err;

    res.send({
      signedstring : sign(stringtosign, {
        key : aws.AWS_SECRET_ACCESS_KEY,
        regionName : aws.AWS_REGION,
        serviceName : 's3'
      })
    });
  });
});

http.createServer(app).listen(port);

console.log(`[...] localhost:${port}/`);

// Filename: signer.js
// Timestamp: 2019.07.15-12:17:57 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

const crypto = require('crypto-js');

// https://docs.aws.amazon.com/general/latest/gr/sigv4-calculate-signature.html
//
// ex, 20150830
const dateStr = (date = new Date()) => date
  .toISOString().replace(/[:-]|\.\d{3}/g, '').substr(0, 8);

// https://docs.aws.amazon.com/general/latest/gr/sigv4-calculate-signature.html
//
const createSigningKey = (opts, d = opts.date) => {
  const dateStamp = typeof d === 'string' ? d : dateStr(d || new Date());
  const kDate = crypto.HmacSHA256(dateStamp, `AWS4${opts.key}`);
  const kRegion = crypto.HmacSHA256(opts.regionName, kDate);
  const kService = crypto.HmacSHA256(opts.serviceName, kRegion);

  return crypto.HmacSHA256('aws4_request', kService);
};

const sign = (str, opts) => crypto
  .HmacSHA256(str, createSigningKey(opts)).toString();

module.exports = sign;
module.exports.createSigningKey = createSigningKey;
module.exports.dateStr = dateStr;
